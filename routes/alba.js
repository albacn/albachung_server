/**
 * Created by globalsmartkr on 2015. 7. 29..
 */
var express = require('express');
var util = require('./util');
var router = express.Router();

var db_model = require('./db_model');
var member = require('./member');


function searchAlbaInfo(token, review, alba, alba_comment, tag_array, res) {

    //  get username from database.
    member.getUserName(token, function(name) {

        //  when user is not exist.
        if(!name)
            res.status(405).json({
                message: 'ERROR: Invalid access_token.'
            });

      //  search alba info is exists.
      db_model.Alba.findOne()
        .where('place_id').equals(alba.place_id)
        .exec(function(err, alba_data) {
            //  save alba info
            if(err)
                console.log(err);
            else if(!alba_data) {
                var albainfo = new db_model.Alba({
                    place_id: alba.place_id,
                    name: alba.name,
                    subname: alba.subname,
                    address: alba.address,
                    contact: alba.contact,
                    latitude: alba.latitude,
                    longitude: alba.longitude
                });

                albainfo.save(function(err, result) {
                    if(!err) {
                        //  save alba review.
                        saveReview(result._id, name, review, alba_comment, tag_array, res);
                    } else {
                        res.status(500).json({
                        message: 'Failed to save alba review.'
                        });
                    }
                });
            } else {
                saveReview(alba_data._id, name, review, alba_comment, tag_array, res);
            }
        });

    });


}

function saveReview(alba_id, username, review, comment, tag, res) {

    var review_data = new db_model.Review({
        post_time: Date.now(),
        edit_time: Date.now(),
        username: username,
        alba: alba_id,
        review: {
            boss: review.boss,
            money: review.money,
            work: review.work,
            time: review.time,
            client: review.client,
            partner: review.partner
        },
        comment: comment,
        tag: tag
    });

    review_data.save(function(err, data) {
        console.log('result: ' + JSON.stringify(data));
        if(err) {
            console.error(err);
            res.status(500).json({
                message: 'Failed to save while alba review save into db.'
            });
        } else {
            //  Send OK status.
            res.status(200).json({
              message: 'Succeeded to save alba review!'
            });

            //  update it's alba tag information.
            updateTag(data.alba._id, data.tag);

        }
    });
}

/**
 * Update one place's tag information
 *
 * @param alba_id where to update tag schema.
 * @param tags review tag String array.
 */
function updateTag(alba_id, tags) {

   	db_model.Alba.findOne()
	   	.select('tag')
	    .where('_id').equals(new mongoose.Types.ObjectId(alba_id))
	    .exec(function(err, tag_array) {
	        if (err) {
	            console.log(err);
	        } else if(tag_array != null) {
                //  increase count all of the exist tag.
                //  create and set count to 1 for not used tag.

                //  Check all of review's tag if it is exists in alba schema.
                for (tagKey in tags) {
                  //  for this, when tag doesn't exist in tagSchema, create new one.
                  var exist = false;
                  for(albaTagKey in tag_array) {
                      //  increase exists tag count.
                      if(tags[tagkey] == tag_array[albaTagKey].name) {
                          tag_array[albaTagKey].count++;
                          exist = true;
                      }
                  }
                  //  tag is not in the tagSchema, then create new one.
                  if(!exist)
                      tag_array.push(new tagSchema({
                          name: tags[tags[tagKey]],
                          count: 1
                      }));
                }
                //  save changes.
                tag_array.save();
            }
	    });
	

}


function findAlba(reqlat, reqlng){
  	//distance search
  	var albas = {};

  	db_model.Alba.find()
  		.exec(function(err, alba) {
  			if (err) { console.log(err); throw err;}
  			var distance = util.DistanceByMeter(reqlng, reqlat, alba.latitude, alba.longitude);
  			if ( distance <= 10000) {//10,000meter
  				//albas[alba] = distance;
  			}
  		});
  	//albas sort and send albas.JSON
}

function findAlba(reqlat, reqlng) {
  	//distance search
  	db_model.Alba.find()
  		.exec(function(err, alba) {
  			if (err) { console.log(err); throw err;}
  			var distance = util.DistanceByMeter(reqlng, reqlat, alba.latitude, alba.longitude);
  			if ( distance <= 10000) {//10,000meter
  				albas[alba._id] = distance;
  			}
  			res.send(alba);
  		});
  	
}

function findAlba(name, subname) {
	//to do
}


/**
 * Receive Alba Review
 *
 * @param token to recognize user.
 * @param place where you worked.
 * @param time when request received.
 * @param tags array of tag.
 */
router.route('/review')
    .post(function(req, res) {
        req.accepts('application/json');
        console.log('Post Alba Review Request Detected.');
        var json_param = req.body.nameValuePairs;
        console.log('RAW JSON = ' + JSON.stringify(json_param));
        var token = json_param.token;
        var review_json = json_param.review;
        var alba_json = json_param.alba;
        var comment = json_param.comment;
        var tag_array = json_param.tag;
        if(token == undefined || review_json == undefined || alba_json == undefined) {
            res.status(405).send({
                message: "Some params are not found."
            });
        } else {
            searchAlbaInfo(token, review_json, alba_json, comment, tag_array, res);
        }
    });


router.route('/reviews')
    .get(function(req, res) {
        console.log('Get Alba Request Detcted.');
        var name = req.params.name;
        //  지점명
        var subname = req.params.subname;
        //  알바 종류(Ex. 편의점, 배달), 주소
        var category = req.params.category;
        var address = req.params.address;
        var contact = req.params.contact;
        //  위도, 경도
        var latitude = req.params.latitude;
        var longitude = req.params.longitude;
        var tag = req.params.tag;
        //scope search base on current position
        if ( name && subname)
        	findAlba(name, subname);
        if ( latitude &&  longitude )
        	findAlba(latitude, longitude);

        //findAlba(name, subname, category, address, contact, latitude, longitude, tag);
    });

module.exports = router;