var express = require('express');
var mongoose = require('mongoose');
//  DB Models object
var db_model = require('./db_model');


var logRequest = function (req, res, next) {
    getInfo(req, function () {
        console.log('request information saved.');
    });
    next();
};



function getInfo(req, callback) {
    var ip = req.connection.remoteAddress;
    var url = req.protocol + '://' + req.get('host') + req.originalUrl;
    save(ip, url, callback);
}

function save(ip, reqURL, callback) {
    var logData = new db_model.Log({
        time: Date.now(),
        ip: ip,
        request: reqURL
    });
    logData.save(function(err, log) {
        if(err)
            console.log('ERROR save log.');
        else {
            console.log(log);
            callback();
        }
    });
}

function getBasicErrorResponse(data) {
    return data ? {
        message: 'an error occurred.',
        data: data
    } : {
        message: 'an error occurred.'
    };
}

module.exports = logRequest;
module.exports.basicErrorResponse = getBasicErrorResponse;