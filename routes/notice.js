var mongoose = require('mongoose');
var express = require('express');
var router = express.Router();
var Notice = require('./db_model').Notice;

router.route('/list')
	.get(function(req, res) {
		console.log('Get Notice list detected.');
		Notice.find().exec(function(err, result) {
			if(!err && result) {
				var array = [];
				for(idx in result) {
					console.log(result[idx]);
					array.push({
						title: result[idx].title,
						url: result[idx].url
					});
				}	// for end

				res.status(200).json({
					data: array
				});	// end of json response
			}
			});
	});

router.route('/')
	.post(function(req, res) {
		var req_title = req.body.title;
		var notice_num = 0;
		Notice.find().exec(function(err, notice) {
			if(!err && notice != null)
				notice_num = notice.length;
			else if(notice == null)
				notice_num = 0;
				var notice = new Notice({
					title: req_title,
					url: "http://175.158.15.181/" + (notice_num + 1)
				});
				notice.save(function(err, notice) {
					if(!err && notice != null)
						res.json({
							message: "Succeeded to save notice.",
							data: notice
						});
				});

		});
	})
	.delete(function(req, res) {
		var req_post_num = req.body.post_num;
		Notice.find()
			.where("url").equals("http://175.158.15.181/" + req_post_num)
			.remove().exec(function(err, notice) {
				if(!err)
					res.json({
						message: "Succeeded to delete " + req_post_num + " post."
					});
				else
					res.status(404).json({
						message: "Unable to delete " + req_post_num + " post."
					});
			});
	});


module.exports = router;