var mongoose = require('mongoose');
var crypto = require('crypto');

var Member = require('./db_model').Member;

/**
 * Check user current token's effectiveness
 * by device(or other somethings) unique id.
 * If it is turned out invalid, api request
 * are going to cancel.
 * @param token current user's api access token
 * @param key device unique id to contrast with token.
 */
function checkEffectiveness(token, key) {
    //  TODO DB connect and query using token and key.
    //  TODO Add this to middleware and export.
}

module.exports.createUserToken = function createUserToken(callback) {
    crypto.randomBytes(48, function(ex, buf) {
        callback(buf.toString('hex'));

        //  Except token save.
        //  Just return created token.
        /**
        Member.findOne({id: id}, function(err, member) {
            if(err || ! member) {
                console.log(err);
            }
            now = new Date();
            member.token.access_token = userToken
            member.expires_at = now.setDate(now.getDate() + 7);
        })
         */
    });

}

function encrypt(data, key, callback) {
    var cipher = crypto.createCipher('aes-256-ctr',key);
    var crypted = cipher.update(data, 'utf8', 'hex');
    crypted += cipher.final('hex');
    callback(crypted);
}

function decrypt(crypted, key ,callback) {
    var decipher = crypto.createDecipher('aes-256-ctr', key);
    var decrypt = decipher.update(crypted, 'hex', 'utf8');
    decrypt += decipher.final('utf8');
    callback(decrypt);
}

/**
 * Used to make one user's private location information key.
 * @param sign_up_date when the user is signed up in this service.
 * @param id user database id.
 * @returns {string} the key to use encrypt & decrypt.
 */
function createCryptKey(sign_up_date, id) {
    return sign_up_date + ":" + id;
}

function DistanceByMeter(lat1, lng1, lat2, lng2) {
    var theta = lon1 - lon2;
    dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1))
         * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
    dist = Math.acos(dist);
    dist = rad2deg(dist) * 60 * 1.1515 * 1.609344;   
}

function deg2rad(deg) {
    return (deg * Math.PI / 180);
}

function rad2deg(rad) {
    return (rad * 180  / Math.PI);
}

function sortObjectByValue(obj)
{
  // convert object into array
    var sortable=[];
    for(var key in obj)
        if(obj.hasOwnProperty(key))
            sortable.push([key, obj[key]]); // each item is an array in format [key, value]

    // sort items by value
    sortable.sort(function(a, b)
    {
      return a[1]-b[1]; // compare numbers
    });
    return sortable; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
}

module.exports = this;