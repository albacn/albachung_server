var mongoose = require('mongoose');


var logSchema = new mongoose.Schema({
    time: {
        type: String,
        default: Date.now
    },
    ip: {
        type: String,
        default: '0.0.0.0'
    },
    request: String
});

var tagSchema = new mongoose.Schema({
    name: {
        type: String,
        lowercase: true
    },
    count: {
        type: Number,
        default: 0
    }
});

var albaSchema = new mongoose.Schema({
    place_id: String,
    name: String,
    //  지점명
    subname: String,
    //  알바 종류(Ex. 편의점, 배달)
    category: String,
    address: String,
    contact: String,
    //  위도
    latitude: Number,
    //  경도
    longitude: Number,
    tag: [tagSchema]
});


var reviewSchema = new mongoose.Schema({
    post_time: {
        type: Date,
        default: Date.now
    },
    edit_time: {
        type: Date,
        default: Date.now
    },
    username: {
        type: String,
        default: ''
    },
    /**
     * 아르바이트 장소 정보를 객체 아이디로 갖고 있는다. 
    */
    alba: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Alba'
    },
    /**
     * 아르바이트 리뷰에 대한 평가 항목
     * 사장 / 시급 / 일의 강도 / 근무 시간 / 손님 / 다른 알바생
     * 에 대한 항목으로 구성되어 있다.
     */
    review: {
        boss: {
            type: Number,
            min: 0,
            max: 5,
            default:0
        },
        money: {
            type: Number,
            min: 0,
            max: 5,
            default: 0
        },
        work: {
            type: Number,
            min: 0,
            max: 5,
            default: 0
        },
        time: {
            type: Number,
            min: 0,
            max: 5,
            default: 0
        },
        client: {
            type: Number,
            min: 0,
            max: 5,
            default: 0
        },
        partner: {
            type: Number,
            min: 0,
            max: 5,
            default: 0
        }
    },
    comment: {
        type: String,
        default: ''
    },
    tag: {
        type: [String]
    }
});

var memberSchema = new mongoose.Schema({
    id: String,
    userpw: String,
    name: String,
    email: String,
    profile_img: String,
    birthday: {
        type: Date,
        default: Date.now
    },
    gender: {
        type: String,
        default: 'male'
    },
    //  Social Account Connection Info
    //  If it is connected, values have social account internal id.
    social_connect: {
        facebook: String
    },
    token: {
        access_token: String,
        refresh_token: String,
        expires_at: Date
    },
    fb: {
        access_token: String,
        expires_at: Number
    },
    signed_up_date: {
        type: Date,
        default: Date.now
    },
    last_login: {
        type: Date,
        default: Date.now
    }
});

var noticeSchema = new mongoose.Schema({
    title: {
        type: String,
        default: ''
    },
    url: {
        type: String,
        default: 'http://175.158.15.181/'
    }
});

module.exports.Log = mongoose.model('Log', logSchema);
module.exports.Review = mongoose.model('Review', reviewSchema);
module.exports.Member = mongoose.model('Member', memberSchema);
module.exports.Alba = mongoose.model('Alba', albaSchema);
module.exports.Tag = mongoose.model('Tag', tagSchema);
module.exports.Notice = mongoose.model('Notice', noticeSchema);