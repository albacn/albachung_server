var express = require('express');
var routes = express.Router();
var request = require('request');
var FB = require('fb');
var util = require('./util');

var Member = require('./db_model').Member;

/**
 * Get the facebook 3'rd party application access token.
 * The token that obtained from this function is used
 * when inspecting client-side FB API access token.
 *
 * NOTE: However, App Access Token is never expired,
 * so we don't use this function, replace actual app access token directly.
 */
function getApAccessToken() {
    FB.api('/oauth/access_token', {
        client_id: '1033052363396097',
        client_secret: 'bc10db671fc312ef8fee6c8989075ca2',
        grant_type: 'client_credentials'
    }, function(res) {
        if(!res || res.error) {
            console.log(!res ? 'FB OAuth Error' : res.error);
            return;
        }
        return res.access_token;
    });
}

/**
 * Inspect User's Access Token obtained from Client-side FB Login.
 * By inspecting token, server can confirm the token is valid
 * by performing OAuth to FB Server.
 *
 * @param token Client-side Access Token.
 */
function inspectToken(token, callback) {
    var parseToken = function(response) {
        var result = JSON.parse(response);
        if(!result.data.is_valid) {
            console.log('FB Error: invalid user access_token.');
            return {
                token: '',
                expires_at: ''
            };
        }
        return {
            user_id: result.data.user_id,
            expires_at: result.data.expires_at
        };
    };
    var APP_ACCESS_TOKEN = '1033052363396097|kgUTQFzAbxgIWQNGcmakScv0wTA';

    request('https://graph.facebook.com/debug_token?' +
        'input_token=' + token +
        '&access_token=' + APP_ACCESS_TOKEN, function(err, response, body) {
        if(!err && response.statusCode == 200) {
            console.log('token is valid.');
            callback(parseToken(body));
        } else if(err) {
            console.log('Unable to connect FB Server.'  + err);
            callback();
        } else {
            console.log('Status ' + response.statusCode + '\n token ' + token + ' is not valid.')
            callback();
        }
    });
}

/**
 * Get the FB User Information using FB access_token.
 * @param token FB access_token
 */
function register(token, fbData) {

    //  Create access_token and save userdata.
    var saveData = function(userInfo, fbData) {
        util.createUserToken(function(app_access_token) {
            var now = new Date();
            var userData = new Member({
                id: userInfo.id,
                name: userInfo.name,
                gender: userInfo.gender,
                profile_img: 'https://graph.facebook.com/' + userInfo.id + '/picture?type=large',
                social_connect: {
                    facebook: userInfo.id
                },
                token: {
                    access_token: app_access_token,
                    expires_at: now.setDate(now.getDate() + 7)
                },
                fb: {
                    access_token: fbData.token,
                    expires_at: fbData.expires_at
                },
                signed_up_date: Date.now(),
                last_login: Date.now()
            }); //  End of Member schema

            //  Save the User Data
            userData.save(function(err, user) {
                if(err) {
                    console.log('ERROR: saving user data while register process.');
                    res.status(400).json({
                        message: 'Failed to register.'
                    });
                }
                res.json({
                    access_token: user.token.access_token,
                    expires_at: user.token.expires_at,
                    user: {
                        username: user.name,
                        profile_image: user.profile_img
                    }
                });
            }); //  End of User Data saving
        }); //  End of create token.
    }


    request('https://graph.facebook.com/me?' +
        'fields=id,name,gender&' +
        'access_token=' + token, function(err, response, body) {
        if(!err && response.statusCode == 200) {
            saveData(JSON.parse(body), fbData);
        } else {
            console.log('ERROR getting FB User Info.');
            res.send(400).json({
                message: 'Error getting Facebook User information.'
            });
        }
    });

}

routes.route('/test')
    .get(function(req, res) {
	   res.send('알바청 서버 테스트 페이지입니다.\n서버 잘 돌아가고 있습니다!');
    });

routes.route('/login')
    .all(function(req, res) {
        console.log(Date.now() + ': Login Request Detected.');
        res.send('Sorry! We only support social login.');
    });

/**
 * @param token Client-side FB Access Token.
 * @param user_id FB user id that will use to match user data.
 */
routes.route('/login/facebook')
    .post(function(req, res) {
        var token = req.body.token;
        var user_id = req.body.user_id;
        console.log('Login request user_id: ' + user_id + '\n' +
            'FB access_token: ' + token);
        //  Inspect the given token is valid.
        inspectToken(token, function(data) {
            if(data != undefined && user_id == data.user_id) {
                console.log('User ID is equal');

                Member.findOne().where('id').equals(user_id).exec(function(err, user) {
                    if(err)
                        console.log('ERROR: FB login failed. user_id=' + user_id);
                    //  User is not exist.
                    //  Automatically sign up this service.
                    else if(!user) {
                        //  Request User Information to register.
                        register(token, data);
                    }
                    //  User is signed up.
                    //  We only perform Login
                    else {
                        console.log('Performing login.');
                        util.createUserToken(function(token) {
                            now = new Date();
                            user.token.access_token = token;
                            user.token.expires_at = now.setDate(now.getDate() + 7);
                            user.last_login = Date.now();
                            user.save(function(err) {
                                if(err) {
                                    console.log('ERROR: login failed. access_token not update to db');
                                    res.status(400).json({
                                        message: 'Login failed.'
                                    });
                                } else {
                                    res.json({
                                        access_token: user.token.access_token,
                                        expires_at: user.token.expires_at,
                                        user: {
                                            username: user.name,
                                            profile_img: user.profile_img
                                        }
                                    });
                                }

                            }); //  End of db save.
                        }); //eEnd of create token
                    }   //  End of login existing user.
                }); //  End of find userdata in database and perform login.
            }   // End of check token valid.

            //  Login failed.
            else {
                res.status(400).json({
                    message: 'Login failed'
                });
            }
        }); //  End of inspect Token.
    });

routes.route('/register')
    .post(function(req, res) {
        console.log('register does not supported.');
        res.status(400).send('Sorry, we do not supported register. Please use social login.');
    });

/**
 * Get Username By the given token.
 * Return Username to callback function.
 * @param token
 * @param callback
 */
function getUserNameByToken(token, callback) {
    Member.findOne()
        .select('name')
        .where('token.access_token').equals(token)
        .exec(function(err, name) {
            if(err)
                console.error(err);
            else if(!name)
                callback();
            else
                callback(name.name);
        });
}


module.exports = routes;
module.exports.getUserName = getUserNameByToken;
