var express = require('express');
var path = require('path');
var logger = require('morgan');
var body_parser = require('body-parser');

//  MongoDB connect module.
var mongoose = require('mongoose');

//  Login / Register Manager
var member = require('./routes/member');
//  알바청 review module.
var alba = require('./routes/alba');
var albalog = require('./routes/log');
var notice = require('./routes/notice');

var app = express();

app.use(logger('dev'));
app.use(body_parser.urlencoded({  extended: true }));
app.use(body_parser.json());
app.use(albalog);
app.use('/alba', alba);
app.use('/notice', notice);
app.use('/', member);


//  Handle Errors.
app.use(function(err, req, res, next) {
  console.error('Error: ' + err.stack);
  res.status(500).send('500 Error.<p>Something broke!');
});


/**
 * Connect db and start server.
 * @param port database port and server port.
 */
function connectDB(port) {
  //  Server DB
  //mongoose.connect('mongodb://10.32.14.162:54321/infodb', function(err) {
  //  Test DB
  mongoose.connect('mongodb://localhost/infodb', function(err) {
    if(err) {
      console.error('failed to connect db.\n' + err);
      throw err;
    }
    console.log('successfully connected.');
    app.listen(port, function () {
      console.log("server is running at port " + port);
    });
  });
};

//  Connect DB and if is succeeded, start Web server.
connectDB(3000);

module.exports = app;
